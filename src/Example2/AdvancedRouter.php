<?php

namespace Workshop\Solid\Example2;

use Workshop\Solid\Example2\RouterInterface;
use Workshop\Solid\Example2\Route;
use Workshop\Solid\Example2\RouteCollection;

class AdvancedRouter implements RouterInterface
{
    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        $routeCollection = new RouteCollection();

        $routeCollection
            ->addRoute(new Route('/', 'Homepage:default'))
            ->addRoute(new Route('/history', 'Message:history'));

        return $routeCollection;
    }
}

