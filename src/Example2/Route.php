<?php

namespace Workshop\Solid\Example2;

class Route
{
    /** @var string */
    private $route;

    /** @var string */
    private $controller;

    /**
     * @param string $route
     * @param string $controller
     */
    public function __construct($route, $controller)
    {
        $this->route      = $route;
        $this->controller = $controller;
    }
}

