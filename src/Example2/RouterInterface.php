<?php

namespace Workshop\Solid\Example2;

use Workshop\Solid\Example2\Route;

interface RouterInterface
{
    /**
     * @return Route[]
     */
    public function getRoutes();
}

