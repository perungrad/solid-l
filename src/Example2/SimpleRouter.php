<?php

namespace Workshop\Solid\Example2;

use Workshop\Solid\Example2\RouterInterface;
use Workshop\Solid\Example2\Route;

class SimpleRouter implements RouterInterface
{
    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        $routes = [];

        $routes[] = new Route('/', 'Homepage:default');
        $routes[] = new Route('/history', 'Message:history');

        return $routes;
    }
}

