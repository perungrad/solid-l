<?php

namespace Workshop\Solid\Example2;

use Workshop\Solid\Example2\Route;

class RouteCollection implements \Iterator
{
    /** @var array */
    private $routes = [];

    /** @var integer */
    private $position = 0;

    /**
     * @param Route $route
     *
     * @return self
     */
    public function addRoute(Route $route)
    {
        $this->routes[] = $route;

        return $this;
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return $this->routes[$this->position];
    }

    /**
     * @return integer
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return void
     */
    public function next()
    {
        $this->position += 1;
    }

    /**
     * @return void
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return boolean
     */
    public function valid()
    {
        return isset($this->routes[$this->position]);
    }
}

