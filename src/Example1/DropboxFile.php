<?php

namespace Workshop\Solid\Example1;

use Workshop\Solid\Example1\FileInterface;

class DropboxFile implements FileInterface
{
    /**
     * @param string $name
     */
    public function rename($name)
    {
        // ...
    }

    /**
     * @param string $user
     * @param string $group
     */
    public function changeOwner($user, $group)
    {
        throw new \BadMethodCallException(
            'Not implemented for Dropbox files'
        );
    }
}

