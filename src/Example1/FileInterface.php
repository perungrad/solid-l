<?php

namespace Workshop\Solid\Example1;

interface FileInterface
{
    /**
     * @param string $name
     */
    public function rename($name);

    /**
     * @param string $user
     * @param string $group
     */
    public function changeOwner($user, $group);
}
