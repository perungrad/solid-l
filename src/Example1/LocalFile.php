<?php

namespace Workshop\Solid\Example1;

use Workshop\Solid\Example1\FileInterface;

class LocalFile implements FileInterface
{
    /** @var string */
    private $filepath;

    /**
     * @param string $filepath
     */
    public function __construct($filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @param string $name
     */
    public function rename($name)
    {
        rename($this->filepath, $name);

        $this->filepath = $name;
    }

    /**
     * @param string $user
     * @param string $group
     */
    public function changeOwner($user, $group)
    {
        chown($this->filepath, $user);
        chgrp($this->filepath, $group);
    }
}

