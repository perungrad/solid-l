<?php

namespace Workshop\Solid\Example4;

use Workshop\Solid\Example4\Request;

interface HttpKernelInterface
{
    /**
     * @param Request $request
     */
    public function handle(Request $request);
}

