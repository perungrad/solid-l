<?php

namespace Workshop\Solid\Example4;

use Workshop\Solid\Example4\HttpKernelInterface;
use Workshop\Solid\Example4\Request;

class HttpKernel implements HttpKernelInterface
{
    /**
     * @param Request $request
     */
    public function handle(Request $request)
    {
        // ...
    }

    /**
     * @return string
     */
    public function getEnvironment()
    {
        // ...
    }
}

