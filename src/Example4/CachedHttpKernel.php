<?php

namespace Workshop\Solid\Example4;

use Workshop\Solid\Example4\HttpKernelInterface;
use Workshop\Solid\Example4\Request;

class CachedHttpKernel implements HttpKernelInterface
{
    /** @var HttpKernelInterface */
    private $kernel;

    /**
     * @param HttpKernelInterface $kernel
     */
    public function __construct(HttpKernelInterface $kernel)
    {
        if ($kernel->getEnvironment() === 'dev') {
            // ...
        }

        $this->kernel = $kernel;
    }

    /**
     * @param Request $request
     */
    public function handle(Request $request)
    {
        // ...
    }
}

