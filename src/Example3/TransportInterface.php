<?php

namespace Workshop\Solid\Example3;

use Workshop\Solid\Example3\MessageInterface;

interface TransportInterface
{
    /**
     * @param MessageInterface $message
     * @param string           $recipient
     */
    public function transport(MessageInterface $message, $recipient);
}

