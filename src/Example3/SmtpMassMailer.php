<?php

namespace Workshop\Solid\Example3;

use Workshop\Solid\Example3\TransportInterface;
use Workshop\Solid\Example3\MessageInterface;
use Workshop\Solid\Example3\RecipientsInterface;
use Workshop\Solid\Example3\SmtpTransport;
use Workshop\Solid\Example3\MassMailerInterface;

class SmtpMassMailer implements MassMailerInterface
{
    /**
     * @param TransportInterface  $transport
     * @param MessageInterface    $message
     * @param RecipientsInterface $recipients
     */
    public function sendMail(TransportInterface $transport, MessageInterface $message, RecipientsInterface $recipients)
    {
        if (!($transport instanceof SmtpTransport)) {
            throw new \InvalidArgumentException(
                'SmtpMassMailer only works with SMTP'
            );
        }

        // ...
    }
}

