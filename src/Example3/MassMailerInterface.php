<?php

namespace Workshop\Solid\Example3;

use Workshop\Solid\Example3\TransportInterface;
use Workshop\Solid\Example3\MessageInterface;
use Workshop\Solid\Example3\RecipientsInterface;

interface MassMailerInterface
{
    /**
     * @param TransportInterface  $transport
     * @param MessageInterface    $message
     * @param RecipientsInterface $recipients
     */
    public function sendMail(TransportInterface $transport, MessageInterface $message, RecipientsInterface $recipients);
}

