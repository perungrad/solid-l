<?php

namespace Workshop\Solid\Example3;

use Workshop\Solid\Example3\TransportInterface;
use Workshop\Solid\Example3\MessageInterface;

class SmtpTransport implements TransportInterface
{
    /**
     * @param MessageInterface $message
     * @param string           $recipient
     */
    public function transport(MessageInterface $message, $recipient)
    {
        // ...
    }

    /**
     * @param MessageInterface $message
     * @param array<string>    $recipients
     */
    public function massTransport(MessageInterface $message, $recipients)
    {
        // ...
    }
}

